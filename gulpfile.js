// Require

var gulp    = require('gulp'),
    gutil   = require('gulp-util'),
    sass    = require('gulp-sass'),
    concat  = require('gulp-concat'),
    prefix  = require('gulp-autoprefixer'),
    postcss = require('gulp-postcss'),
    rename  = require('gulp-rename'),
    uglify  = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps');
    cleanCSS = require('gulp-clean-css');
    tailwindcss = require('tailwindcss');

gulp.task('css', function () {
  return gulp.src('assets/styles/main.css')
    .pipe(postcss([
      tailwindcss('tailwind.config.js'),
      require('autoprefixer'),
    ]))
    .pipe(cleanCSS({compatibility: 'ie11'}))
    .pipe(gulp.dest('web/assets/css'));
});

gulp.task('scripts', function() {
  return gulp.src(['assets/js/*.js'])
  .pipe(concat('main.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('web/assets/js'));
});

gulp.task('watch', function () {
  gulp.watch('assets/styles/*.css', gulp.series('css'));
  gulp.watch('tailwind.config.js', gulp.series('css'));
  gulp.watch('assets/scripts/*.js', gulp.series('scripts'));
});
