module.exports = {
  purge: [],
  theme: {
      fontFamily: {
        display: ['orpheuspro', 'serif'],
        //body: ['Open Sans', 'sans-serif'],
      },
      colors: {
          green: '#4f5a4c',
      },
      textColor: {
          white: '#fff',
          grey: '#e2e8f0',
      },
    extend: {},
  },
  variants: {},
  plugins: [],
}
